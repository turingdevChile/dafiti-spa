# vue-dafiti

> Dafiti prototype shopping


## Build Setup & Run

``` bash
#clone repo
git clone https://turingdevChile@bitbucket.org/turingdevChile/dafiti-spa.git
#go to the folder project & install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).


# Sobre el proyecto
La carpeta /dist almacena el build del proyecto, para servir el proyecto en produccion, solo se necesitan 
el index.html y la carpeta /dist

Según lo conversado desarrolle una spa con Vue, me tomé  la libertad de desarrollar un poco más de lo que me pidieron y 
implemente un carro de compra con todas las funcionalidades (Agregar, eliminar, ver detalle) y sus componentes reactivos, 
para esto use Vuex que es el equivalente a Redux de react, Ambos son manejadores de estados, esta herramienta básicamente 
dicho en resumidas palabras nos permite manejar variables globales reactivas dentro de nuestra aplicación y con esto 
construí mi carro de compras. En la carpeta /src/store/ se encuentran dos archivos, en uno declaro mis mutaciones mutation-types.js y en el otro index.js resuelvo la lógica principal de la app.
El código está bastante claro, cualquier duda podemos conversar por skype y explico el flujo de la app. 

Me gustaria señalar que la app fue desarrollada de forma dinaminca, si le damos una fuente (archivo json) con más productos, 
esta lista para operar con ellos.

PD: La app desde el celu se ve cool. https://dafiti.ml


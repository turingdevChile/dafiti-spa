import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types'
import json from '../assets/detalle.json'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

// initial state
const state = {
  added: [],
  json_pr: json.products
}

// getters
const getters = {
  jsonProducts: state => state.json_pr,
	getNumberOfProducts: state => (state.json_pr) ? state.json_pr.length : 0,
	cartProducts: state => {
		return state.added.map(({ id, quantity }) => {
			const product = state.json_pr.find(p => p.id === id)

			return {
        id: product.id,
				name: product.productname,
				price: product.originalprice,
        image: product.otherimages,
				quantity
			}
		})
	}
}

// actions
const actions = {
	addToCart({ commit }, product){
		commit(types.ADD_TO_CART, {
			id: product.id
		})
  },
  removeToCart({ commit }, product){
    commit(types.REMOVE_TO_CART, {
      id: product.id
    })
  },
} //End actions

// mutations
const mutations = {

	[types.ADD_TO_CART] (state, { id }) {
	    const record = state.added.find(p => p.id === id)

	    if (!record) {
	      state.added.push({
	        id,
	        quantity: 1
	      })
	    } else {
	      record.quantity++
	    }
	  },
  [types.REMOVE_TO_CART](state, { id }){

    const record = state.added.find(p => p.id === id)

    if (record.quantity > 1) {
      record.quantity--
    } else {
      state.added.splice(record,1);
    }
  }
}

// one store for entire application
export default new Vuex.Store({
	state,
	strict: debug,
	getters,
	actions,
	mutations
})
